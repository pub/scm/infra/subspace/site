lists.linux.dev
===============

.. csv-table::
   :widths: 30, 47, 2, 18, 3
   :header-rows: 1
   :stub-columns: 1
   :file: _listinfo/lists.linux.dev.csv

Flags
-----
* **a**: announcements-only list with restricted posting
* **s**: only subscribers may post
* **m**: subscribers and/or posts to the list are moderated

Requesting list hosting
-----------------------
If you would like to host a mailing list on lists.linux.dev, please send
a request to helpdesk@kernel.org describing your needs. Please adhere to
the following example template::

   Subject: Create somesuch@lists.linux.dev

   Address    : somesuch@lists.linux.dev
   Description: Linux somesuch device drivers
   Owners     : dev1@addr.email [, dev2@addr.email]
   Allow HTML : y/N
   Archives   : Y/n

   Reasons for the list and additional info:
   We need a separate list for somesuch device drivers.

If list posting needs to be restricted to subscribers or moderated,
please include that into the request together with the explanation why
this is required (usually reserved for announcement-type lists).

.. note::
   Since "linux" is already in the domain, there is generally no reason
   to name the list linux-somesuch@lists.linux.dev, unless you are
   migrating an existing list from another location or have some other
   convincing need to include linux- in the name.

.. important::
   We can only host mailing lists dedicated to Linux development.

Contacts
--------
To report abuse of with any other requests, please email
postmaster@kernel.org.
