linux.kernel.org
================

This is a small subset of lists that, for historical reasons, don't live
together with the rest of vger.kernel.org.

.. csv-table::
   :widths: auto
   :header-rows: 1
   :stub-columns: 1
   :file: _listinfo/linux.kernel.org.csv

Flags
-----
* **a**: announcements-only list with restricted posting
* **s**: only subscribers may post
* **m**: subscribers and/or posts to the list are moderated
