lists.linuxfoundation.org
=========================

While most lists.linuxfoundation.org lists are managed via Mailman,
there is a small subset that is passed through to the subspace system.

.. csv-table::
   :widths: auto
   :header-rows: 1
   :stub-columns: 1
   :file: _listinfo/lists.linuxfoundation.org.csv

Flags
-----
* **a**: announcements-only list with restricted posting
* **s**: only subscribers may post
* **m**: subscribers and/or posts to the list are moderated
