Subspace mailing list server
============================
Subspace is a mailing list service tailored to patch-based developer
workflows. It aims to deliver mail to subscribers without violating any
of the DMARC attestation requirements.

Hosted domains
--------------
.. toctree::
   :maxdepth: 1

   linux.kernel.org
   lists.linux.dev
   lists.linuxfoundation.org
   vger.kernel.org

Important information
---------------------
.. toctree::
   :maxdepth: 1

   subscribing
   etiquette
   software

Contacts
--------
To report abuse of with any other requests, please email
postmaster@kernel.org.
