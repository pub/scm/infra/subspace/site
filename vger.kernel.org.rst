vger.kernel.org
===============

.. csv-table::
   :widths: 30, 47, 2, 18, 3
   :header-rows: 1
   :stub-columns: 1
   :file: _listinfo/vger.kernel.org.csv

Flags
-----
* **a**: announcements-only list with restricted posting
* **s**: only subscribers may post
* **m**: subscribers and/or posts to the list are moderated

What happened to majordomo?
---------------------------
Majordomo commands are no longer supported after the migration away from
legacy vger infrastructure. Please use the "sub/unsub" links in the
table above to subscribe to a mailing list and see this page for further
information:

- https://subspace.kernel.org/subscribing.html

Requesting list hosting
-----------------------
We are not currently accepting new list hosting requests for
vger.kernel.org. If you would like a Linux related mailing list hosted
on this platform, please request it under the lists.linux.dev domain:

* `lists.linux.dev <https://subspace.kernel.org/lists.linux.dev.html>`_

Contacts
--------
To report abuse of with any other requests, please email
postmaster@kernel.org.
